<?php
require 'logging.php';
require 'download.php';
require 'util.php';
require 'ziptool.php';
require 'package.php';


/*

  打包要做以下工作

  - 去掉PHP文件中的注释
    - 两个配置文件不修改注释
    - 所有文件头部的版权声明不去掉

  - JS文件
    - 去掉注释
    - 版权声明保留

  - 对于繁体
    - 替换图片
    - 所有文本文件语言换成繁体
    - css中的字体，simsum换成mingliu
    - 设置config_global_default.php中 $_config['output']['language'] = 'zh_tw'
    - 所有文件中，宋体->细明体，新宋体->新细明体, simsun->mingliu, 楷体_GB2312->标楷体
    - install/include/install_var.php中的 INSTALL_LANG

  - 其它处理
    - admincp_checktools.php中加一行
    - 更新版本号
    - 生成各语言的md5文件
 */

 $G_RELEASE_REVISION = "";
 $G_HAS_ZIP_COMMAND = true;

function doWork($branch, $work_dir, $zip_dir, $argv)
{
    global $G_RELEASE_REVISION;

    $EXTRACTED_DIR = join_paths($work_dir, 'extracted/');
    $EXTRACTED_DISCUZX_DIR = join_paths($EXTRACTED_DIR, 'DiscuzX-'. $branch .'/');
    $MINIFIED_DIR = join_paths($work_dir, 'minified');
    $DOWNLOAD_DIR = join_paths($work_dir, 'download');
    $DIST_DIR = join_paths($work_dir, 'dist');


    // 删除并重建工作目录
    if (in_array("--clean", $argv)) {
        rrmdir($work_dir);
    }

    try_mkdir($work_dir);
    try_mkdir($DOWNLOAD_DIR);
    try_mkdir($DIST_DIR);
    try_mkdir($EXTRACTED_DIR);
    try_mkdir($zip_dir);

    // 下载并解压缩
    if (!in_array("--skip-download", $argv))
        download_branch_zip($branch, join_paths($DOWNLOAD_DIR, $branch . ".zip"));

    if (!in_array("--skip-unzip", $argv)) {
        extract_zip(join_paths($DOWNLOAD_DIR, $branch . '.zip'), $EXTRACTED_DIR);
        // 清理解压缩后的结果
        rrmdir($EXTRACTED_DISCUZX_DIR . "/upload/uc_server/release/20080429");
        rrmdir($EXTRACTED_DISCUZX_DIR . "/upload/uc_server/upgrade");
        rmfile($EXTRACTED_DISCUZX_DIR . "/upload/api/addons/zendcheck.php");
        rmfile($EXTRACTED_DISCUZX_DIR . "/upload/api/addons/zendcheck52.php");
        rmfile($EXTRACTED_DISCUZX_DIR . "/upload/api/addons/zendcheck53.php");
    }


    // 清理PHP文件中的注释
    strip_all_php_files($EXTRACTED_DISCUZX_DIR, $MINIFIED_DIR);

    // 清理JavaScript文件中的注释。.min.js结尾的文件不二次处理
    strip_all_js_files($EXTRACTED_DISCUZX_DIR, $MINIFIED_DIR);

    // 所有非PHP，非JS文件，复制到目标目录
    copy_all_other_files($EXTRACTED_DISCUZX_DIR, $MINIFIED_DIR);

    fix_files_misc($MINIFIED_DIR);

    // 准备打包

    foreach (array('SC_UTF8', 'TC_UTF8', 'SC_GBK', 'TC_BIG5') as $encoding) {
        // 根据社区讨论结果, Discuz! X3.5 不再生产本地化编码版本
        // Todo: X3.4 停止支持后, v3.5 -> master 之前修改此处, 避免再次打包出 X3.5 本地化版本
        if ($branch != 'master' && in_array($encoding, array('SC_GBK', 'TC_BIG5'))) {
            continue;
        }
        $encoding_dir = join_paths($DIST_DIR, $encoding);
        $zip_name = sprintf("DZX-%s-%s.zip", $encoding, $G_RELEASE_REVISION);
        if (!in_array("--skip-package", $argv))
            prepare_package($encoding, $MINIFIED_DIR, $encoding_dir);
        if (!in_array("--skip-zip", $argv))
            make_zip(join_paths($zip_dir, $zip_name), $encoding_dir);
    }

    if (!in_array("--skip-cleanup", $argv)) {
      rrmdir($work_dir);
    }
}

if (function_exists('date_default_timezone_set')) {
    @date_default_timezone_set('Etc/GMT-8');
}

// 检查所需extension
check_necessary_extension();
check_usage($argv);


$branch = $argv[1];
$work_dir = $argv[2];
$zip_dir = $argv[3];

logging\info("start to package %s", $branch);
logging\info("working directory %s", $work_dir);
logging\info("zip output to %s", $zip_dir);

if (($last_hash = get_last_commit_id($branch)) === FALSE) {
    logging\error("cannot get last commit hash of %s", $branch);
    exit(1);
}
$time_str = get_release_date();

$G_RELEASE_REVISION = sprintf("%s-%s-%s", $branch == 'master' ? 'v3.4' : $branch, $time_str, $last_hash);
logging\info("release revision is %s", $G_RELEASE_REVISION);

check_if_already_done($zip_dir);

doWork($branch, $work_dir, $zip_dir, $argv);