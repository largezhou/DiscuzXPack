<?php

function generate_discuzfiles_md5($target_path)
{
    logging\info("calculating md5 %s", $target_path);
    $md5data = array();
    $md5data += checkfiles($target_path, './', '', 0);
    $md5data += checkfiles($target_path, 'api/', '\.php|\.htm|\.pem');
    $md5data += checkfiles($target_path, 'archiver/', '\.php', 0);
    $md5data += checkfiles($target_path, 'config/', '\.php|\.htm', 1, 'config_global.php,config_ucenter.php');
    $md5data += checkfiles($target_path, 'data/', '\.xml|\.htm', 0);
    $md5data += checkfiles($target_path, 'data/avatar/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'data/log/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'data/cache/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'data/ipdata/', '\.htm|\.dat', 0);
    $md5data += checkfiles($target_path, 'data/template/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'data/plugindata/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'data/threadcache/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'data/download/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'data/addonmd5/', '\.htm', 0);
    $md5data += checkfiles($target_path, 'extend/', '\.php|\.htm');
    $md5data += checkfiles($target_path, 'm/', '\.php');
    $md5data += checkfiles($target_path, 'source/', '\.php|\.md5|\.htm|\.table|\.xml', 0);
    $md5data += checkfiles($target_path, 'source/admincp/', '\.php|\.md5|\.htm|\.table|\.xml|\.dat|\.txt', 1, 'discuzfiles.md5');
    $md5data += checkfiles($target_path, 'source/archiver/', '\.php|\.md5|\.htm|\.table|\.xml');
    $md5data += checkfiles($target_path, 'source/class/', '\.php|\.md5|\.htm|\.table|\.xml');
    $md5data += checkfiles($target_path, 'source/function/', '\.php|\.md5|\.htm|\.table|\.xml');
    $md5data += checkfiles($target_path, 'source/include/', '\.php|\.md5|\.htm|\.table|\.xml');
    $md5data += checkfiles($target_path, 'source/language/', '\.php|\.md5|\.htm|\.table|\.xml');
    $md5data += checkfiles($target_path, 'source/module/', '\.php|\.md5|\.htm|\.table|\.xml');
    $md5data += checkfiles($target_path, 'static/', '\.js|\.png|\.css|\.jpg|\.gif|\.txt|\.htm|\.ico|\.swf|\.ttf|\.mp3|\.xml');
    $md5data += checkfiles($target_path, 'template/', '\.php|\.htm|\.css|\.jpg|\.xml|\.gif|\.png|\.eot|\.svg|\.ttf|\.woff|\.js');
    $md5data += checkfiles($target_path, 'uc_client/', '\.php|\.htm', 0);
    $md5data += checkfiles($target_path, 'uc_client/data/', '\.htm');
    $md5data += checkfiles($target_path, 'uc_client/control/', '\.php|\.htm');
    $md5data += checkfiles($target_path, 'uc_client/model/', '\.php|\.htm');
    $md5data += checkfiles($target_path, 'uc_client/lib/', '\.php|\.htm');
    $md5data += checkfiles($target_path, 'uc_server/', '\.php|\.htm|\.txt|\.xml', 0);
    $md5data += checkfiles($target_path, 'uc_server/data/', '\.htm');
    $md5data += checkfiles($target_path, 'uc_server/api/', '\.php|\.htm');
    $md5data += checkfiles($target_path, 'uc_server/control/', '\.php|\.htm|\.md5');
    $md5data += checkfiles($target_path, 'uc_server/model/', '\.php|\.htm');
    $md5data += checkfiles($target_path, 'uc_server/lib/', '\.php|\.htm');
    $md5data += checkfiles($target_path, 'uc_server/plugin/', '\.php|\.htm|\.xml');
    $md5data += checkfiles($target_path, 'uc_server/upgrade/', '\.php');
    $md5data += checkfiles($target_path, 'uc_server/images/', '\..+?');
    $md5data += checkfiles($target_path, 'uc_server/js/', '\.js|\.htm');
    $md5data += checkfiles($target_path, 'uc_server/release/', '\.php');
    $md5data += checkfiles($target_path, 'uc_server/view/', '\.php|\.htm');
    
    $savedatanew = '';
    foreach($md5data as $file => $md5) {
        $savedatanew .= $md5.' *'.$file."\r\n";
    }
    $file_name = join_paths($target_path, 'source/admincp/discuzfiles.md5');
    logging\info("writing md5 to %s", $file_name);
    file_put_contents($file_name, $savedatanew);
}

function checkfiles($target_path, $currentdir, $ext = '', $sub = 1, $skip = '') {
	$md5data = array();
	$dir = @opendir(join_paths($target_path, $currentdir));
	if(empty($dir)) return $md5data;
	$exts = '/('.$ext.')$/i';
	$skips = explode(',', $skip);

	while($entry = readdir($dir)) {
        $file = $currentdir . $entry;
        $full_file = join_paths($target_path, $file);
		if($entry != '.' && $entry != '..' && (preg_match($exts, $entry) || $sub && is_dir($full_file)) && !in_array($entry, $skips)) {
		    if($sub && is_dir($full_file)) {
		        $md5data += checkfiles($target_path, $file . '/', $ext, $sub, $skip);
		    } else {
		        if(is_dir($full_file)) {
		            $md5data[$file] = md5($file);
		        } else {
		            $md5data[$file] = md5_file($full_file);
		        }
		    }
		}
    }
    
    return $md5data;
}
