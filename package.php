<?php

require_once "process_file_php.php";
require_once "process_file_js.php";
require_once "process_file.php";
require_once "md5files.php";

function prepare_package($encoding, $path, $target_path)
{
	if (!in_array($encoding, array('SC_UTF8', 'TC_UTF8', 'SC_GBK', 'TC_BIG5'))) {
		logging\error("unknown encoding %s", $encoding);
		exit(1);
	}

	logging\info("---- preparing for %s ----", $encoding);
	cpdir($path, $target_path);

	// ipdata
	$src = join_paths("dzfile/ipdata", $encoding, "tinyipdata.dat");
	$target = join_paths($target_path, "upload/data/ipdata/tinyipdata.dat");
	logging\info("copying tinyipdata.dat %s -> %s", $src, $target);
	copy($src, $target);

	if (starts_with($encoding, "TC_")) {
		// 繁体图片
		$src = "dzfile/image_big5";
		$target = join_paths($target_path, "upload/static/image");

		logging\info("copying traditional chinese images", $src, $target);
		copy_all_files($src, $target);

		convert_to_hant($target_path);
	}
	
	// 转换编码
	if (ends_with($encoding, "_GBK")) {
		convert_encoding_in($target_path, 'gbk');
	}
	if (ends_with($encoding, "_BIG5")) {
		convert_encoding_in($target_path, 'big5');
	}

	if ($encoding != 'SC_UTF8') {
		replace_encoding_variables($target_path, $encoding);	
	}

	generate_discuzfiles_md5(join_paths($target_path, 'upload'));
}

?>